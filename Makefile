
.DEFAULT_GOAL=all
.PHONY: clean

clean:
	rm -v os
all:
	gcc -static -o3 os.c -o os
	chmod +x os
install:
	cp os ~/bin
